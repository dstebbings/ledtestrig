/*
////////////////////////////////////////////////////////////
Project: Point Control LED
Element: Test Rig
Author: Dane Stebbings - 5 Wits Productions
Date: 04/16/2018

This is code for the Teensy 3.1 programmed in the Arduino IDE using
Teensyduino. It controls RGB pixels with the WS2812 chip.

The lighting uses the 8 available channels through the OCTOWS2811
library.

////////////////////////////////////////////////////////////
*/

#include <OctoWS2811.h>

#define ledsPerStrip 300
#define frameDelay 12

#define RED_INPUT 17
#define GREEN_INPUT 18
#define BLUE_INPUT 19
#define RAINBOW_INPUT 22

const int pixelConfig = WS2811_GRB | WS2811_800kHz;
int displayMemory[6*ledsPerStrip];
DMAMEM int drawingMemory[6*ledsPerStrip];
OctoWS2811 leds(ledsPerStrip, displayMemory, drawingMemory, pixelConfig);
int rainbowColors[180];

void setup() {
  pinMode(RED_INPUT, INPUT_PULLUP);
  pinMode(GREEN_INPUT, INPUT_PULLUP);
  pinMode(BLUE_INPUT, INPUT_PULLUP);
  pinMode(RAINBOW_INPUT, INPUT_PULLUP);

  for (int i=0; i<180; i++) {
    int hue = i * 2;
    int saturation = 100;
    int lightness = 50;
    // pre-compute the 180 rainbow colors
    rainbowColors[i] = makeColor(hue, saturation, lightness);
  }
  
  leds.begin();
  leds.show();
  Serial.begin(9600);
}

void loop() {
  int r,g,b = 0;
  long color;
  static long frameTimer;

  
  if( (long)millis() - frameTimer >= 0){    //If frame is ready to fire...
    frameTimer = (long)millis() + frameDelay;

    if(! digitalRead(RAINBOW_INPUT)){
      rainbow();
    }
    else{
      if(! digitalRead(RED_INPUT)) r = 255;
      else r = 0;

      if(! digitalRead(GREEN_INPUT)) g = 255;
      else g = 0;
      
      if(! digitalRead(BLUE_INPUT)) b = 255;
      else b = 0;
      
      color = (r << 16) + (g << 8) + b;
      
      for (int i=0; i < ledsPerStrip*8 ; i++) {
        leds.setPixel(i, color);
      }
    }
    leds.show();
  }
}

void rainbow(){
  static int color;
  int x, y, offset;
  
  for (int i=0; i < ledsPerStrip*8 ; i++) {
    int index = (color + i) % 180;
    leds.setPixel(i, rainbowColors[index]);
  }
  color = (color + 1) % 180;
}

int makeColor(unsigned int hue, unsigned int saturation, unsigned int lightness){
  unsigned int red, green, blue;
  unsigned int var1, var2;

  if (hue > 359) hue = hue % 360;
  if (saturation > 100) saturation = 100;
  if (lightness > 100) lightness = 100;

  // algorithm from: http://www.easyrgb.com/index.php?X=MATH&H=19#text19
  if (saturation == 0) {
    red = green = blue = lightness * 255 / 100;
  }
  else{
    if (lightness < 50) {
      var2 = lightness * (100 + saturation);
    }
    else{
      var2 = ((lightness + saturation) * 100) - (saturation * lightness);
    }
    var1 = lightness * 200 - var2;
    red = h2rgb(var1, var2, (hue < 240) ? hue + 120 : hue - 240) * 255 / 600000;
    green = h2rgb(var1, var2, hue) * 255 / 600000;
    blue = h2rgb(var1, var2, (hue >= 120) ? hue - 120 : hue + 240) * 255 / 600000;
  }
  return (red << 16) | (green << 8) | blue;
}

unsigned int h2rgb(unsigned int v1, unsigned int v2, unsigned int hue){
  if (hue < 60) return v1 * 60 + (v2 - v1) * hue;
  if (hue < 180) return v2 * 60;
  if (hue < 240) return v1 * 60 + (v2 - v1) * (240 - hue);
  return v1 * 60;
}
